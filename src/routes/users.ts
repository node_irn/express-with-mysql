import express, { Request, Response, NextFunction } from 'express';

const userRouter = express.Router();

export default userRouter;

userRouter.get('/', (req: Request, res: Response, next: NextFunction) => { res.send('user list'); });
userRouter.post('/login', (req: Request, res: Response, next: NextFunction) => { res.send(`user login`); next() });
userRouter.post('/register', (req: Request, res: Response, next: NextFunction) => { res.send(`user register`); next() });
userRouter.get('/:userId([0-9]+)', (req: Request, res: Response, next: NextFunction) => { res.send(`profile view of user ${req.params.userId}`); next() });
userRouter.put('/:userId([0-9]+)', (req: Request, res: Response, next: NextFunction) => { res.send(`profile edit of user ${req.params.userId}`); next() });
userRouter.get('/:userId([0-9]+)/my-contact', (req: Request, res: Response, next: NextFunction) => { res.send(`my contact of user ${req.params.userId}. ${req.query.gender ? `Filter gender: ${req.query.gender}.` : ''} ${req.query.nationality ? `Filter nationality: ${req.query.nationality}.` : ''}`); next() });
userRouter.post('/:userId([0-9]+)/logout', (req: Request, res: Response, next: NextFunction) => { res.send(`logout of user ${req.params.userId}`); next() });