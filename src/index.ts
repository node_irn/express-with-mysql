import express, { Request, Response, NextFunction } from 'express';
import dotenv from 'dotenv';
import moment from 'moment';
import { userRouter } from './routes';

dotenv.config();

const app = express();
const PORT = process.env.APP_PORT;

app.use('/', (req: Request, res: Response, next: NextFunction) => {
  console.log(`Log: API call received ${moment(Date.now()).format('dddd, MMMM Do YYYY, h:mm:ss a')}`);
  next();
});

app.use('/user', userRouter);

app.use('/', (req: Request, res: Response) => {
  console.log(`Log: API call ended ${moment(Date.now()).format('dddd, MMMM Do YYYY, h:mm:ss a')}`);
});

app.listen(PORT, () => {
  console.log(`⚡️[server]: Server is running at http://localhost:${PORT}`);
});
